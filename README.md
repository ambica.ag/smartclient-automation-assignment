## clone this repo to a local directory
git clone https://gitlab.com/ambica.ag/smartclient-automation-assignment

## cd into the cloned repo
cd smartclient-automation-assignment

## install the node_modules
npm install

## start the local cypress runner
npx cypress open

## to run all the test
Click Run 3 integration specs on right top corner

## to run individual test
Click individual spec to run test


