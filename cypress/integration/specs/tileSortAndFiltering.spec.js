describe('Tile Sort And Filtering Test', () => {
    
    before(function () {
        cy.fixture('tileSortAndFiltering').then((values) => {
            this.data = values
        })
    })

    it('Filter animal tiles', function() {
        cy.visit('/#featured_tile_filtering')
        cy.get(this.data.animalInputBoxCSS).type('a')
        cy.get(this.data.lifespanSliderButton)
            .click()
            .trigger("mousedown", { which: 1, pageX: 0, pageY: 0 });
        cy.get(this.data.lifespanSliderTrack)
            .trigger("mousemove", 144, 0)
            .trigger("mouseup");
        cy.get(this.data.sortDropdownArrowCSS).click()
        cy.get(this.data.sortDropdownValueCSS).find('td').contains('Life Span').click()
        cy.xpath(this.data.ascendingCheckboxXpath).click()
        cy.wait(1000)
        cy.xpath(this.data.filteredTilesXpath).then(($el) => {
            expect($el.length).to.be.greaterThan(12)
        })
    })
  
})

