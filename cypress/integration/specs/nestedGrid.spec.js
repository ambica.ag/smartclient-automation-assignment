import { generateRandomString } from '../../support/commonUtils'

describe('Nested Grid Test', () => {
     
    before(function () {
        cy.fixture('nestedGrid').then((values) => {
            this.data = values
        })
    })

    it('change description of Correction results', function() {
        cy.visit("/#featured_nested_grid")
        cy.xpath(this.data.itemHeaderXpath).click()
        cy.xpath(this.data.itemFilterDropdownXpath).click()
        cy.xpath(this.data.showFilterXpath).click()
        cy.xpath(this.data.inputTextXpath).type("Correction{enter}")
        cy.wait(1000)
        cy.xpath(this.data.searchResultRecordsXpath).then(rows => {
            const rowCount = rows.length
            for (var i = 1; i <= rowCount; i++) {
                cy.xpath(this.data.searchResultRecordsXpath+"[" + i + "]").then($el => {
                    cy.wrap($el).should('be.visible').children().first().click()
                    cy.wait(1000)
                    cy.xpath(this.data.subItemsRecordsXpath)
                        .each(($el, index) => {
                            if ($el.text() != "No items to show.") {
                                const counter = ++index;
                                const desc = "[aria-posinset='" + counter + "'] > :nth-child(3) > div"
                                cy.get(desc).click()
                                cy.xpath(this.data.descriptionTextAreaXpath).as("textarea")
                                cy.get("@textarea").clear()
                                cy.get("@textarea").type(counter + " " + generateRandomString(10))
                                cy.xpath(this.data.saveButtonXpath).click()
                            }
                        })
                    cy.xpath(this.data.closeButtonXpath).click()
                })
            }

        })
    })
})
