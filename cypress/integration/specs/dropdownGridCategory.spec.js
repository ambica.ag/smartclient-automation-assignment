
describe('Dropdown Grid Category Test', () => {

    before(function () {
        cy.fixture('dropdownGridCategory').then((values) => {
            this.data = values
        })
    })

    it('find item from dropdown', function () {
        cy.visit("/#featured_dropdown_grid_category")
        cy.xpath(this.data.itemDropdownXpath).click().type('exer', { force: true })
        cy.wait(100)
        cy.xpath(this.data.itemDropdownXpath).click()
        cy.wait(500)
        cy.xpath(this.data.filterItemUnitsXpath)
            .each(($el) => {
                if ($el.text() > 1.1) {
                    cy.wrap($el).click()
                    return false
                }
            })
        cy.xpath(this.data.itemDropdownXpath).then(($el) => {
            expect($el.text()).to.equal("Exercise Book Sewn 240 Page")
        })
     })

})